
import csv
import json

csvfile = open('results.csv', 'r')
jsonfile = open('data.json', 'w')

fieldnames = ('curso','universidad','fuente','enlace','categoria')
reader = csv.DictReader( csvfile, fieldnames)
jsonfile.write('[')
for row in reader:
    json.dump(row, jsonfile)
    jsonfile.write(',')
jsonfile.write(']')
