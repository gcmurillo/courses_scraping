import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JsonService } from '../services/json/json.service';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FilterPipe } from './filter.pipe';
import { FormsModule, ReactiveFormsModule, FormGroup, Validators } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
  ],
  providers: [JsonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
