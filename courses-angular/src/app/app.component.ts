import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JsonService } from '../services/json/json.service';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  categorias = [];
  universidades = [];
  fuentes = [];
  rows: any[];


  curso = '';
  universidad = '';
  fuente = '';
  categoria = '';

  get_data() {
    this.categorias = [];
    this.universidades = [];
    this.fuentes = [];
    this.fetch((data) => {
      this.rows = data;
      this.categorias.push('');
      this.universidades.push('');
      this.fuentes.push('');
      for (const i of data) {
        if (!this.categorias.includes(i.categoria)) {
          this.categorias.push(i.categoria);
        }
        if (!this.fuentes.includes(i.fuente)) {
          this.fuentes.push(i.fuente);
        }
        if (!this.universidades.includes(i.universidad)) {
          this.universidades.push(i.universidad);
        }
      }
    });
  }

  constructor() {
    this.get_data();
    console.log(this.categorias);
  }

  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  change_categoria() {
    if (this.categoria !== '') {
      this.fuentes = [];
      this.universidades = [];
      this.universidades.push('');
      this.fuentes.push('');
      for (const i of this.rows) {
        if (i.categoria === this.categoria) {
          if (!this.fuentes.includes(i.fuente)) {
            this.fuentes.push(i.fuente);
          }
          if (!this.universidades.includes(i.universidad)) {
            this.universidades.push(i.universidad);
          }
        }
      }

    } else {
      this.get_data();
    }
  }

  change_fuente() {
    if (this.fuente !== '') {
      this.categorias = [];
      this.universidades = [];
      this.universidades.push('');
      this.categorias.push('');
      for (const i of this.rows) {
        if (i.fuente === this.fuente) {
          if (!this.categorias.includes(i.categoria)) {
            this.categorias.push(i.categoria);
          }
          if (!this.universidades.includes(i.universidad)) {
            this.universidades.push(i.universidad);
          }
        }
      }

    } else {
      this.get_data();
    }
  }

  change_universidad() {
    if (this.universidad !== '') {
      this.categorias = [];
      this.fuentes = [];
      this.fuentes.push('');
      this.categorias.push('');
      for (const i of this.rows) {
        if (i.universidad === this.universidad) {
          if (!this.categorias.includes(i.categoria)) {
            this.categorias.push(i.categoria);
          }
          if (!this.fuentes.includes(i.fuente)) {
            this.fuentes.push(i.fuente);
          }
        }
      }

    } else {
      this.get_data();
    }
  }

}
