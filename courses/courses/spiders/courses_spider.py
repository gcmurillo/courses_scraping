import re
import json

from scrapy import Spider, Item, Field

class CourseItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    titulo = Field()
    duracion = Field()
    categoria = Field()
    fuente = Field()


class Course(Spider):

    def __init__(self):
        file = open('results.csv', 'w')
        cadena = 'curso,universidad,fuente,enlace,categoria\n'
        file.write(cadena)
        file.close()

    name = "Courses"


    coursera_urls = [
        'https://www.coursera.org/browse/data-science',
        'https://www.coursera.org/browse/business',
        'https://www.coursera.org/browse/personal-development',
        'https://www.coursera.org/browse/computer-science',
        'https://www.coursera.org/browse/information-technology',
        'https://www.coursera.org/browse/language-learning',
        'https://www.coursera.org/browse/life-sciences',
        'https://www.coursera.org/browse/math-and-logic',
        'https://www.coursera.org/browse/physical-science-and-engineering',
        'https://www.coursera.org/browse/social-sciences',
        'https://www.coursera.org/browse/arts-and-humanities'
    ]

    miriadax_url = [
        'https://miriadax.net/cursos?_3_formDate=1537058773607&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=89888203&_3_selCategory=89888203&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_45603%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46504%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47101%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47002%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46502%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46801%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47001%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40904%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48102%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50605%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_44804%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50903%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_51001%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50910',
        'https://miriadax.net/cursos?_3_formDate=1537059705404&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=10581&_3_selCategory=10581&_3_selLang=&_3_groupId=&_3_zzwc_facet=&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_47201',
        'https://miriadax.net/cursos?_3_formDate=1537061205686&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=11165&_3_selCategory=11165&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_48306%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48305%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46302%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_32701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42606%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37403%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_36708%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_36901%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39705%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37402%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39503%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_25301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37903',
        'https://miriadax.net/cursos?_3_formDate=1537061394539&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=11150&_3_selCategory=11150&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_46801%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48102%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49319%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43704%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43902%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_38401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_44302%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_25401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_32701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42005%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_32201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_41804%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_20101%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_26701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_26301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_36011',
        'https://miriadax.net/cursos?_3_formDate=1537061440196&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=89887812&_3_selCategory=89887812&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_51008%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49509%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47403%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37003%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_36901%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40809%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_38303%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39603%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_23902%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_21602%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_30704%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_30705%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_30706',
        'https://miriadax.net/cursos?_3_formDate=1537061449806&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=89887794&_3_selCategory=89887794&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_46502%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46305%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48702%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48104%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49509%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48103%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47201',
        'https://miriadax.net/cursos?_3_formDate=1537061475299&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=89887818&_3_selCategory=89887818&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_46801%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50605%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_45501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49916%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48702%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49509',
        'https://miriadax.net/cursos?_3_formDate=1537061746581&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=94725786&_3_selCategory=94725786&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_47501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50605%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_45501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49916%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49509%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47201',
        'https://miriadax.net/cursos?_3_formDate=1537061781408&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=11157&_3_selCategory=11157&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_46502%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46801%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50605%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_44804%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48703%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_49319%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_47202%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_32201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39309%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_31505%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_30501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_14515%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_20602%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_19901',
        'https://miriadax.net/cursos?_3_formDate=1537061804136&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=94725781&_3_selCategory=94725781&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_45501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_46304%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_45801%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48104%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_45802%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42402%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48103%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43502%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42112%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42301%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_41307%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40903%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40704%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40112%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_38503%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_32707',
        'https://miriadax.net/cursos?_3_formDate=1537061830739&p_p_id=3&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=&_3_courseStatus=&_3_yzjm_facet=&_3_tipology=&_3_tfyc_facet=&_3_assetCategoryIds=94725780&_3_selCategory=94725780&_3_selLang=&_3_groupId=&_3_zzwc_facet=&_3_itemsPerPage=5&_3_page=1&_3_itemsPerPage=5&_3_page=1&documentsSearchContainerPrimaryKeys=courseadmin_WAR_liferaylmsportlet_PORTLET_50601%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48701%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48104%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_42402%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_48103%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_43201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_40806%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39401%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_37501%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_39307%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_35901%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_22502%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_34205%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_25901%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_11302%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_20201%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_18704%2Ccourseadmin_WAR_liferaylmsportlet_PORTLET_20502',

    ]

    url_categories_miriada = [
        'turismo',
        'ciencias de la salud',
        'Ciencias de las Artes y las Letras',
        'Ciencias de la Vida',
        'Ciencias Sociales',
        'Comunicacion',
        'Desarrollo Personal',
        'Docencia',
        'Economia',
        'Emprendimiento',
        'Empresas'
    ]

    start_urls = miriadax_url + coursera_urls + edx_urls

    def get_extracted(value, index=0):
        try:
            return value[index]
        except:
            return ""


    def parse(self, response):

        file = open('results.csv', 'a+')
        print('Inicio parseo')
        print('url: ' + response.request.url)
        if response.request.url in self.coursera_urls:
            course_name = response.xpath('//div[contains(@class, "rc-ProductCardWithMicroDP")]//div[contains(@class, "titleSection")]//h4//div[contains(@style, "overflow")]//div/text()').extract()
            # print(course_name)
            university_name = response.xpath('//div[contains(@class, "titleSection")]//div[contains(@class, "weightNormal")]//div[contains(@style, "overflow")]//div/text()').extract()
            # print(university_name)
            enlaces = response.xpath('//div[contains(@class, "rc-ProductCardWithMicroDP")]//div//a/@href').extract()
            categoria = str(response.request.url).split('/')[-1]
            # print('Cursos: ' + str(len(course_name)))
            # print('Universidades: ' + str(len(university_name)))
            # print('Enlaces: ' + str(len(enlaces)))
            for i in range(len(course_name)):
                cadena = course_name[i].replace(',', ';') + ',' + university_name[i].replace(',', ';') + ',coursera,https://www.coursera.org' + enlaces[i] + ',' + categoria + '\n'
                print('Curso: ' + course_name[i].replace(',', ';') + '\nUniversidad: ' + university_name[i] + '\nEnlace: https://www.coursera.org' + enlaces[i] + '\nCategoria: ' + categoria + '\n\n')
                file.write(cadena)

        elif 'miriadax' in response.request.url:
            indice = self.miriadax_url.index(response.request.url)
            categoria = self.url_categories_miriada[indice]
            course_name = response.xpath('//div[@class="curs-title"]//div[@class="name"]//a/text()').extract()
            university_name = response.xpath('//div[@class="curs-title"]//div[@class="university"]//a/text()').extract()
            enlaces = response.xpath('//div[@class="curs-title"]//div[@class="name"]//a/@href').extract()
            for i in range(len(course_name)):
                cadena = course_name[i].replace(',', ';') + ',' + university_name[i].replace(',', ';') + ',miriadax,https://www.miriadax.net' + enlaces[i] + ',' + categoria + '\n'
                print('Curso: ' + course_name[i].replace(',', ';') + '\nUniversidad: ' + university_name[i] + '\nEnlace: https://www.miriadax.net' + enlaces[i] + '\nCategoria: ' + categoria + '\n\n')
                file.write(cadena)

        file.close()
